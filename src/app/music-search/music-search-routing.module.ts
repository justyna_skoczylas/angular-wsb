import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlbumSearchViewComponent } from './container/album-search-view/album-search-view.component';

const routes: Routes = [
  {
    path: 'search/albums',
    component: AlbumSearchViewComponent
  },
  {
    path: 'search',
    redirectTo: 'search/albums',
    pathMatch: 'prefix'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicSearchRoutingModule { }
