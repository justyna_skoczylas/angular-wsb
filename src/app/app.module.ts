import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlaylistsModule } from './playlists/playlists.module';
import { CoreModule } from './core/core.module';
import { MusicSearchModule } from './music-search/music-search.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    CoreModule,
    // features
    PlaylistsModule,
    MusicSearchModule,
    // NgbModule
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent/* ,HeaderComponent,SidebarComponent */]
})
export class AppModule { }
